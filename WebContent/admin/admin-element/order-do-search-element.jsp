<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@taglib prefix="s" uri="/struts-tags"%>
 <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
			<div id="order_do">
			
				<div id="order_do_title">订单处理</div>
				
				<div id="order_do_list">
				
					<div id="order_do_search">
						
						搜索方式:
						<select id="txt_switch" onchange="click_switch()">
							<option value="order_id" >订单编号</option>
							<option value="user_name">用户名</option>
							<option value="all">所有</option>
						</select>
						<input type="text" id="txt_order_id" placeholder="输入订单号:"/>
						<input type="text" id="txt_user_name" placeholder="输入用户名:">
						
						订单状态:<select id="txt_order_state">
									<option value="all">全部</option>
									<option value="no">未处理</option>
									<option value="yes">已发货</option>
							  </select>
						<input type="button" value="搜索" onclick="btn_order_search()"/>
					</div>
					
					<div id="order_do_item">
						<s:if test="#request.orderList != null && #request.orderList.size() > 0">
							<table>
								<tr>
									<th>订单编号</th>
									<th>订单时间</th>
									<th>订单状态</th>
									<th>订单总额(元)</th>
									<th>处理</th>
								</tr>
								<s:iterator id="var_order" value="#request.orderList">
									<tr>
										<td>${var_order. orderId }</td>
										<td>${var_order.orderTime }</td>
										<td>${var_order.orderState }</td>
										<td>${var_order.sumPrice }</td>
										<td>
											<s:if test="#var_order.orderState == '未处理'">
												<input type="button" value="发货" onclick="btn_order_do(${var_order.orderId})"/>
											</s:if>
											<s:else>
												<input type="button" value="退货" onclick="btn_order_back(${var_order.orderId})"/>
											</s:else>
										</td>
									</tr>
								</s:iterator>
							</table>
							
							<div id="order_do_show_page">
							<s:if test="pager.curPage > 1">
								<a href="admin/searchOrder?pager.curPage=1">首页</a>
								<a href="admin/searchOrder?pager.curPage=${pager.curPage - 1 }">上一页</a>
							</s:if>
							<s:if test="pager.curPage < pager.pageCount ">
								<a href="admin/searchOrder?pager.curPage=${pager.curPage + 1 }">下一页</a>
								<a href="admin/searchOrderr?pager.curPage=${pager.pageCount }">尾页</a>
							</s:if>
							总共${pager.rowCount }记录,共${pager.curPage }/${pager.pageCount }页
						</div>
						</s:if>
						<s:else>
							<div id="order_do_no_result">暂无查询信息!</div>
						</s:else>
					</div>
					
				</div>
			</div>
</body>
</html>