<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@taglib prefix="s" uri="/struts-tags"%>
 <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
		<div id="login">
			
			<div id="login_title">登录界面</div>
			
			<div id="login_form">
				
				<div id="login_error">
					<s:actionerror/>
					${requestScope.tip}	
				</div>
				<form action="admin/login" method="post"> 
					<div class="login_item"><input type="text" name="admin.loginName" placeholder="用户名"/></div>
					<div class="login_item"><input type="password" name="admin.loginPassword" placeholder="密码"/></div>
					<div id="btn_login"><input type="submit" value="登录"/></div>
				</form>
				
			</div>
			
		</div>
</body>
</html>