<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@taglib prefix="s" uri="/struts-tags"%>
 <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
		<div id="header_log_img">
			<img alt="" src="image/header_log.png" />
			网上订餐系统
		</div>
		
		<div id="header_navigation">
		
				<ul id="header_admin_ul">
					<li>|&nbsp;<a href="admin/index">网站首页</a>&nbsp;</li>
					<li>|&nbsp;<a href="admin/add_user.jsp">管理员注册</a>&nbsp;</li>
					<li>|&nbsp;<a href="admin/preAddFood" >添加餐品</a>&nbsp;</li>
					<li>|&nbsp;<a href="admin/preDoOrder">订单处理</a>&nbsp;</li>
					<li>|&nbsp;<a href="javaScript:" onclick="admin_login_out()">注销</a>&nbsp;</li>
					<li><font color="#F12C43">欢迎您:${sessionScope.admin.loginName }</font></li>
				</ul>
		</div>
		
		<div id="header_line"></div>
</body>
</html>