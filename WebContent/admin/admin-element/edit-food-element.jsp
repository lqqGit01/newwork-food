<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@taglib prefix="s" uri="/struts-tags"%>
 <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
			<div id="add_food">
			
				<div id="add_food_title">编辑餐品</div>
				
				<div id="add_food_form">
				
					<div id="add_food_error">
					<s:actionerror/>
						<s:fielderror></s:fielderror>
					</div>
					
					<form action="admin/editFood" method="post" enctype="multipart/form-data" id="add_food_form_1">
						<input type="hidden" name="food.foodId" value="${food.foodId }"/>
						<div class="add_food_form_item">
							<span>菜名:</span>
							<div class="add_food_form_item_right">
								<input type="text" name="food.foodName" value="${food.foodName }"/>
							</div>
						</div>
						
						<div class="add_food_form_item">
							<span>菜系:</span>
							<div class="add_food_form_item_right">
								<select name="foodTypeId">
									<s:iterator id="var_food_type" value="#session.foodTypeList">
										<option value="${var_food_type.foodTypeId }">${var_food_type.foodTypeName }</option>
									</s:iterator>
								</select>
							</div>
						</div>
						
						<div class="add_food_form_item">
							<span>摘要:</span>
							<div class="add_food_form_item_right">
								<input type="text" name="food.foodSummary" value="${food.foodSummary }"/>
							</div>
						</div>
						
						<div id="add_food_form_dsc">
								<span>介绍:</span>
								<div class="add_food_form_item_right">
									<textarea rows="5" cols="30" name="food.foodDescription">${food.foodSummary }</textarea>
								</div>
						</div>
						
						<div class="add_food_form_item">
								<span>价格(元):</span>
								<div class="add_food_form_item_right">
									<input type="text" name="food.foodPrice" value="${food.foodPrice }"/>
								</div>
						</div>
						
						<div class="add_food_form_item">
								<span>图片:</span>
								<div class="add_food_form_item_right">
									<input type="file" name="doc"/>
								</div>
						</div>
						
						<div id="add_food_form_submit">
							<input type="submit" value="确定">
						</div>
					</form>
				</div>
			</div>
</body>
</html>