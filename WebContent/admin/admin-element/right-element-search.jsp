<%@page import="java.net.URLEncoder"%>
<%@page import="org.apache.tomcat.util.codec.Encoder"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@taglib prefix="s" uri="/struts-tags"%>
 <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
		
			<div id="content_right_list">
				
				<s:if test="#request.foodList != null && #request.foodList.size() > 0">
					
					<table>
						<tr>
							<th>菜系</th>
							<th>菜名</th>
							<th>摘要</th>
							<th>价格(元)</th>
							<th>修改</th>
							<th>删除</th>
						</tr>
						<s:iterator id="var_food" value="#request.foodList">
							<tr class="food_list_tr">
								<td>
									<s:iterator id="var_food_type" value="#var_food.foodTypes">
										${var_food_type.foodTypeName }&nbsp;
									</s:iterator>
								</td>
								<td class="food_name">${var_food.foodName }</td>
								<td class="food_summary">${var_food.foodSummary }</td>
								<td>${var_food.foodPrice }</td>
								<td><input type="button" value="修改" onclick="modify_food()" /></td>
								<td><input type="button" value="删除" onclick="delete_food()"/></td>
							</tr>
						</s:iterator>
					</table>
						
						<div id="content_right_list_show_page">
							<s:if test="pager.curPage > 1">
								<a href="admin/search?pager.curPage=1&foodName=${requestScope.foodName }">首页</a>
								<a href="admin/search?pager.curPage=${pager.curPage - 1 }&foodName=${requestScope.foodName}>">上一页</a>
							</s:if>
							<s:if test="pager.curPage < pager.pageCount ">
								<a href="admin/search?pager.curPage=${pager.curPage + 1 }&foodName=${requestScope.foodName }">下一页</a>
								<a href="admin/search?pager.curPage=${pager.pageCount }&foodName=${requestScope.foodName }">尾页</a>
							</s:if>
							总共${pager.rowCount }记录,共${pager.curPage }/${pager.pageCount }页
						</div>
						
				</s:if>
				<s:else>
					<font color="red">抱歉，暂无查询信息!</font>
				</s:else>
				
			</div>
			
</body>
</html>