<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@taglib prefix="s" uri="/struts-tags"%>
 <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html >
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/user.css" rel="stylesheet" type="text/css">
<title>登录界面</title>
</head>
<body>
	
		<jsp:include page="admin-element/header.html" />
		
		<div id="content">
			<jsp:include page="admin-element/login-element.jsp" />
		</div>
			
	
</body>
</html>