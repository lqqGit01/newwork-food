<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@taglib prefix="s" uri="/struts-tags"%>
 <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/user.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="js/layer/layer.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/user.js"></script>
<script type="text/javascript">
	$(function(){
		
		$("#add_user_form_1").validate({
			rules:{
				"admin.loginName":{
					required:true
				},
				"admin.loginPassword":{
					required:true
				},
				confirm_password:{
					equalTo:"#txt_password",
					required:true
				}
			},
			messages:{
				"admin.loginName":{required:"用户名必须填写!"},
				"admin.loginPassword":{required:"密码必须填写!"},
				 confirm_password:{
					equalTo:"两次密码操作必须相同!",
					required:"必须填写!"
					}
			},
			
			success:function(label){
				label.text("").addClass("success");
			},
			highlight:function(element, errorClass) {
	            $(element).parent().find("." + errorClass).removeClass("success");
	},
	submitHandler:function(form) {
		var wait = layer.load(3);
		$.post("admin/addUser",
				{
					"admin.loginName":$("#txt_name").val(),
					"admin.loginPassword":$("#txt_password").val()
				},function(data){
					layer.close(wait);
					if(data == "OK"){
						layer.alert("注册成功!是否登录？",
								{
									title:"提示",
									icon:3
								},function(){
									location.href="admin/login.jsp";
								},function(){
									location.href="admin/index";
								});
					}else if(data == "FALSE"){
						layer.alert("已存在用户名为:"+$("#txt_name").val()+"的用户!",
								{
									title:"提示",
									icon:5
								});
					}else{
						layer.alert("注册失败!");
					}
				});
	}
		});
	});
</script>
<title>注册用户</title>
</head>
<body>
		<jsp:include page="admin-element/header.html" />
		
		<div id="content">
			<jsp:include page="admin-element/add-user-element.html" />
		</div>
</body>
</html>