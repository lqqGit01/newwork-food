<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@taglib prefix="s" uri="/struts-tags"%>
 <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>管理员主页</title>
<link href="css/index.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="js/jquery-session/jquerySession.js"></script>
<script type="text/javascript" src="js/layer/layer.js"></script>
<script type="text/javascript" src="js/index.js"></script>
</head>
<body>
		<div id="header">
			<jsp:include page="admin-element/header-element.jsp" />
		</div>
	
		<div id="content">
			
			<div id="content_left">
				
				<jsp:include page="admin-element/left-element.jsp" />
				
			</div>
			
			<div id="content_right">
				<jsp:include page="admin-element/right-element-common.html" />
				<jsp:include page="admin-element/right-element-search.jsp" />
			</div>
			
		</div>
</body>
</html>