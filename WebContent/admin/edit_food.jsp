<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@taglib prefix="s" uri="/struts-tags"%>
 <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/user.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="js/layer/layer.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/user.js"></script>
<title>添加餐品</title>
<script type="text/javascript">

$(function(){
	
	$.validator.addMethod( "my_price", function( value, element ) {
		return this.optional( element ) || /^\d{0,3}(\.\d+)?$/.test( value );
	}, "Please specify a valid postal code" );
	
	$("#add_food_form_1").validate({
		rules:{
			"food.foodName":{
				required:true
			},
			"food.foodSummary":{
				required:true
			},
			"food.foodDescription":{
				required:true
			},
			"food.foodPrice":{
				required:true,
				my_price:true
			}
		},
		messages:{
			"food.foodName":{
				required:"餐品名必须填写!"
			},
			"food.foodSummary":{
				required:"餐品摘要必须填写!"
			},
			"food.foodDescription":{
				required:"餐品描述必须填写!"
			},
			"food.foodPrice":{
				required:"餐品价格必须填写!",
				my_price:"格式不正确!(格式--.--)"
			}
		},
		success:function(label){
			label.text("").addClass("success");
		},
		highlight:function(element, errorClass) {
            $(element).parent().find("." + errorClass).removeClass("success");
	}
	});
});
</script>
</head>
<body>
		<jsp:include page="admin-element/header.html" />
		
		<div id="content">
			<jsp:include page="admin-element/edit-food-element.jsp" />
		</div>
</body>
</html>