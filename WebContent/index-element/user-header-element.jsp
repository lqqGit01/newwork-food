<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@taglib prefix="s" uri="/struts-tags"%>
 <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script type="text/javascript" src="js/jquery/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="js/jquery-session/jquerySession.js"></script>
<title>Insert title here</title>
<script type="text/javascript">
	$(function(){
		if($.session.get("is_login") == null){
			$.session.set("is_login","是");
		}
		
	});
</script>
</head>
<body>

		<div id="header_log_img">
			<img alt="" src="image/header_log.png" />
			网上订餐系统
		</div>
		
		<div id="header_navigation">
		
				<ul id="user_header_ul">
					<li>|&nbsp;<a href="index">网站首页</a>&nbsp;</li>
					<li>|&nbsp;<a href="modifyUser">修改个人信息</a>&nbsp;</li>
					<li>|&nbsp;<a href="showShopCar">我的购物车</a>&nbsp;</li>
					<li>|&nbsp;<a href="showOrders">我的订单</a>&nbsp;</li>
					<li>|&nbsp;<a href="javaScript:" onclick="check_logout()">注销</a>&nbsp;&nbsp;</li>
					<li><font color="#F12C43">欢迎您:${sessionScope.user.realName}</font></li>
				</ul>
				
		</div>
		
		<div id="header_line"></div>
</body>
</html>