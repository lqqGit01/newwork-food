<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@taglib prefix="s" uri="/struts-tags"%>
 <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
			
			<div id="content_right_list">
				
				<s:if test="#request.foodList != null && #request.foodList.size() > 0">
					
						<ul>
						<s:iterator id="var_food" value="#request.foodList" status="st">
							<li>
								<a><img alt="" src="food-images/${var_food.foodImage }" class="show_img"></a>
								<div class="content_right_list_food_desc">
									<span>${var_food.foodName }</span><br /><br />
									<span>价格:${var_food.foodPrice }元</span><br />
									<a href="detail?foods.foodId=${var_food.foodId }">详情</a><a href="javaScript:" onclick="a_add_to_car(${var_food.foodId })">订购</a>
								</div> 
							</li>
						</s:iterator>
						</ul>
						
						<div id="content_right_list_show_page">
							<s:if test="pager.curPage > 1">
								<a href="index?pager.curPage=1&foodTypeId=${resquestScope.foodTypeId }">首页</a>
								<a href="index?pager.curPage=${pager.curPage - 1 }&foodTypeId=${resquestScope.foodTypeId }">上一页</a>
							</s:if>
							<s:if test="pager.curPage < pager.pageCount ">
								<a href="index?pager.curPage=${pager.curPage + 1 }&foodTypeId=${resquestScope.foodTypeId }">下一页</a>
								<a href="index?pager.curPage=${pager.pageCount }&foodTypeId=${resquestScope.foodTypeId }">尾页</a>
							</s:if>
							总共${pager.rowCount }记录,共${pager.curPage }/${pager.pageCount }页
						</div>
						
				</s:if>
				<s:else>
					<font color="red">抱歉，暂无餐品信息!</font>
				</s:else>
				
			</div>
</body>
</html>