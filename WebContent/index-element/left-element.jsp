<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@taglib prefix="s" uri="/struts-tags"%>
 <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>

			<div id="content_left_log_img"></div>
			
			<div id="content_left_line"></div>
			
			<div id="content_left_list">
				
				<s:if test="#request.foodTypeList !=null && #request.foodTypeList.size() > 0">
					<ul>
						<s:iterator id="var_foodType" value="#request.foodTypeList">
							<li><a href="index?foodTypeId=${var_foodType.foodTypeId }">${var_foodType.foodTypeName }</a></li>
						</s:iterator>
					</ul>
				</s:if>
				<s:else>
					<font color="red">抱歉，暂无餐品上架!</font>
				</s:else>
				
			</div>
			
		
</body>
</html>