<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@taglib prefix="s" uri="/struts-tags"%>
 <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
			<div id="content_right_list">
				
				<div id="food_detail">
					<img alt="" src="food-images/${foods.foodImage }">
						<br />
						菜名:<s:property value="foods.foodName"/><br /><br />
						摘要:<s:property value="foods.foodSummary"/><br /><br />
						描述:<s:property value="foods.foodDescription"/><br /><br />
						价格:<s:property value="foods.foodPrice"/>元<br /><br />
						
						分类:<s:iterator id="var_type" value="foods.foodTypes">
								${var_type.foodTypeName }&nbsp;
							</s:iterator>
							<a href="javaScript:" onclick="javaScript:history.back()">返回继续订餐</a>
							
				</div>
				
			</div>
			
		
</body>
</html>