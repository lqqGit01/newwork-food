<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@taglib prefix="s" uri="/struts-tags"%>
 <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<link href="css/user.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="js/jquery-session/jquerySession.js"></script>
<script type="text/javascript" src="js/layer/layer.js"></script>
<script type="text/javascript" src="js/user.js"></script>
<script type="text/javascript">
	$(function(){
		
		$(".txt_quantity").next().hide();
		$(".txt_quantity").bind("input",function(){
			$(this).next().show();
		});
		
	});
	
</script>
</head>
<body>
		<jsp:include page="element/header.html" />
		
		<div id="content">
			<jsp:include page="element/cart-element.jsp" />
		</div>
</body>
</html>