<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@taglib prefix="s" uri="/struts-tags"%>
 <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
		<div id="modify">
		
			<div id="modify_title">修改个人信息</div>
			
			<div id="modify_form">
			
					<form id="modify_form_1" action="">
						<input type="hidden" name="user.userId" value="${user.userId }" id="txt_id">
						<div class="item"><span>用户名</span><input type="text" name="loginName"  value="${user.loginName}" id="txt_loginName" /></div>
						<div class="item"><span>真实姓名</span><input type="text" name="realName" value="${user.realName}" id="txt_realName" /></div>
						<div class="item"><span>邮箱地址</span><input type="text" name="email" value="${user.email}" id="txt_email" /></div>
						<div class="item"><span>手机号码</span><input type="text" name="phone" value="${user.phone }" id="txt_phone"/></div>
						<div class="item"><span>地址</span><input type="text" name="address" value="${user.address}" id="txt_address" /></div>
						<div id="btn_item"><input type="submit" value="修改" id="btn_submit"/></div>
					</form>
					
			</div>
			
		</div>
</body>
</html>