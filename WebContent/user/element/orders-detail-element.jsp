<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@taglib prefix="s" uri="/struts-tags"%>
 <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
		<div id="order_detail">
				
				<div id="order_detail_title">明细订单</div>
				
				<div id="order_detail_list">
					<s:set id="sum_price" value="0" />
						<table>
							<tr>
								<th>明细订单编号</th>
								<th>餐品名称</th>
								<th>订餐数量</th>
								<th>餐品单价(元)</th>
							</tr>
							<s:iterator id="var_order" value="#request.orderDetailList">
								<tr>
									<td>${var_order.ordersAttachId}</td>
									<td>${var_order.foods.foodName}</td>
									<td>${var_order.foodCount}</td>
									<td>${var_order.orderPrice}</td>
									<s:set id="sum_price" value="#sum_price + #var_order.orderPrice" />
								</tr>
							</s:iterator>
							<tr>
								<th colspan="4">总金额${sum_price }元</th>
							</tr>
						</table>
					
					<div id="order_detail_navigation">
						<a href="javaScript:history.back()">返回</a>
					</div>
				</div>
			</div>
</body>
</html>