<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@taglib prefix="s" uri="/struts-tags"%>
 <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/user.css" rel="stylesheet" type="text/css">
<title>订单</title>
</head>
<body>
			<div id="order">
				
				<div id="order_title">我的订单</div>
				
				<div id="order_list">
					<s:if test="#request.orderMainList != null && #request.orderMainList.size() > 0">
						<table>
							<tr>
								<th>订单编号</th>
								<th>订单时间</th>
								<th>订单状态</th>
								<th>查看详细</th>
							</tr>
							<s:iterator id="var_order" value="#request.orderMainList">
								<tr>
									<td>${var_order.orderId }</td>
									<td>${var_order.orderTime }</td>
									<td>${var_order.orderState }</td>
									<td><input type="button" value="查看"  onclick="btn_show_order(${var_order.orderId})"/></td>
								</tr>
							</s:iterator>
						</table>
					</s:if>
					<s:else>
						没有任何订单信息!快去<a href="index">订餐</a>吧
					</s:else>
					
					<div id="order_navigation">
						<a href="javaScript:history.back()">继续订餐</a>
					</div>
				</div>
			</div>
</body>
</html>