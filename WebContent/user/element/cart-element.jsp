<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@taglib prefix="s" uri="/struts-tags"%>
 <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
		<div id="car">
				
				<div id="car_title">购物车</div>
				
				<div id="car_list">
				
					<s:if test="#session.car != null && #session.car.size() > 0">
						<div id="car_item">
							<table>
								<tr>
									<th>餐品编号</th>
									<th>餐品名称</th>
									<th>餐品单价</th>
									<th>餐品数量</th>
									<th>删除</th>
								</tr>
								<s:iterator id="var_item" value="#session.car">
									<tr>
										
										<td>${var_item.value.foods.foodId }</td>
										<td>${var_item.value.foods.foodName }</td>
										<td>${var_item.value.foods.foodPrice }元</td>
										<td class="car_item_td">
											<input type="text" name="quantity"  value="${var_item.value.quantity }" size="2" maxlength="5" onchange="modify_quantity(this.value,this)" class="txt_quantity"/>
											<input type="button" value="修改" onclick="btn_modify(${var_item.value.foods.foodId },this)" class="car_item_td_btn"/>
										</td>
										<td><input type="button" value="删除" onclick="btn_delete(${var_item.value.foods.foodId})"/></td>
									</tr>
								</s:iterator>
							</table>
						</div>
						
						<div id="car_navigation">
							<a href="javaScript:" onclick="a_clear_car()">清空购物车</a>
							<a href="javaScript:history.back()" >继续购物</a>
							<a href="javaScript:" onclick="a_make_order()">生成订单 </a>
						</div>
					</s:if>
					<s:else>
							您的购物车为空哦!快去<a href="index">订餐</a>吧!
					</s:else>
					
				</div>
		</div>
</body>
</html>