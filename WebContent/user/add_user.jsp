<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@taglib prefix="s" uri="/struts-tags"%>
 <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/user.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="js/layer/layer.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/user.js"></script>
<script type="text/javascript">
	$(function(){
		$.validator.addMethod( "my_phone", function( value, element ) {
			return this.optional( element ) || /^(((13[0-9]{1})|(14[0-9]{1})|(17[0]{1})|(15[0-3]{1})|(15[5-9]{1})|(18[0-9]{1}))+\d{8})$/.test( value );
		}, "Please specify a valid postal code" );
		
		$("#add_user_form_1").validate({
			rules:{
				"user.loginName":{
					required:true
				},
				"user.loginPassword":{
					required:true
				},
				confirm_password:{
					equalTo:"#txt_password",
					required:true
				},
				"user.realName":{
					required:true
				},
				"user.phone":{
				    required:true,
				    my_phone:true
				},
				"user.email":{
					required:true,
					email:true
				},
				"user.address":{
					required:true
				}
				
			},
			messages:{
				"user.loginName":{required:"用户名必须填写!"},
				"user.loginPassword":{required:"密码必须填写!"},
				confirm_password:{
					equalTo:"两次密码操作必须相同!",
					required:"必须填写!"
					},
				"user.realName":{required:"真实姓名必须填写!"},
				"user.phone":{
					required:"手机号码必须填写!",
					my_phone:"手机号码不合法!"
						},
				"user.email":{
					required:"邮箱地址必须填写!",
					email:"邮箱地址不合法!"
				},
				"user.address":{required:"地址必须填写!"}
				},
			
			success:function(label){
				label.text("").addClass("success");
			},
			highlight:function(element, errorClass) {
	            $(element).parent().find("." + errorClass).removeClass("success");
	},
	submitHandler:function(form) {
		var wait = layer.load(3);
		$.post("addUser",
				{
					"user.loginName":$("#txt_name").val(),
					"user.loginPassword":$("#txt_password").val(),
					"user.realName":$("#txt_real_name").val(),
					"user.phone":$("#txt_phone").val(),
					"user.email":$("#txt_eamil").val(),
					"user.address":$("#txt_address").val()
				},function(data){
					layer.close(wait);
					if(data == "OK"){
						layer.alert("注册成功!是否登录？",
								{
									title:"提示",
									icon:3
								},function(){
									location.href="user/login.jsp";
								},function(){
									location.href="index";
								});
					}else if(data == "FALSE"){
						layer.alert("已存在用户名为:"+$("#txt_name").val()+"的用户!",
								{
									title:"提示",
									icon:5
								});
					}else{
						layer.alert("注册失败!");
					}
				});
	}
		});
	});
</script>
<title>注册用户</title>
</head>
<body>
		<jsp:include page="element/header.html" />
		
		<div id="content">
			<jsp:include page="element/add-user-element.html" />
		</div>
</body>
</html>