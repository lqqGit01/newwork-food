<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@taglib prefix="s" uri="/struts-tags"%>
 <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script type="text/javascript" src="js/jquery/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="js/layer/layer.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery-session/jquerySession.js"></script>
<script type="text/javascript" src="js/user.js"></script>
<link href="css/user.css" rel="stylesheet" type="text/css">
<title>修改个人信息</title>
<script type="text/javascript">
	$(function(){
		
		$.validator.addMethod( "my_phone", function( value, element ) {
			return this.optional( element ) || /^(((13[0-9]{1})|(14[0-9]{1})|(17[0]{1})|(15[0-3]{1})|(15[5-9]{1})|(18[0-9]{1}))+\d{8})$/.test( value );
		}, "Please specify a valid postal code" );
		
		$("#modify_form_1").validate({
			rules:{
				loginName:{
					required:true
				},
				realName:{
					required:true
				},
				email:{
					required:true,
					email:true
				},
				phone:{
					required:true,
					my_phone:true
				},
				address:{
					required:true
				}
			},
			messages:{
				loginName:{
					required:"请填写用户名!"
				},
				realName:{required:"请填写真实名!"},
				email:{
					required:"请填写邮箱地址!",
					email:"请填写正确的邮箱地址!"
				},
				phone:{
					required:"请填写手机号!",
					my_phone:"请填写正确的手机号!"
				},
				address:{
					required:"请填写地址!"
				}
			},
			success:function(label) {
				    // set &nbsp; as text for IE
				    label.html("&nbsp;").addClass("success");
				    //label.addClass("valid").text("Ok!")
				},
				highlight: function(element, errorClass) {
			            $(element).parent().find("." + errorClass).removeClass("success");
			},
			submitHandler:function(form) {
				layer.confirm("确定要修改吗?",
						{
							title:"提示",
							icon:3
						},function(){
							var wait = layer.load(3);
							$.post("doModifyUser",
									{
										"user.userId":$("#txt_id").val(),
										"user.loginName":$("#txt_loginName").val(),
										"user.realName":$("#txt_realName").val(),
										"user.phone":$("#txt_phone").val(),
										"user.email":$("#txt_email").val(),
										"user.address":$("#txt_address").val()
									},
									function(data){
										layer.close(wait);
										if(data == "OK"){
											layer.alert("修改成功!需要重新登录哦~",
													{
														title:"提示",
														icon:6,
														closeBtn:0
													},
													function(){
														$.session.clear();
														location.href="loginIndex";
													});
										}
									});
						});
			}
			});
	});
</script>
</head>
<body>
		<jsp:include page="element/header.html" />
		
		<div id="content">
			<jsp:include page="element/modify-user-element.jsp" />
		</div>
</body>
</html>