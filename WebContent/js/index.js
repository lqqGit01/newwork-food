
function btn_search(){
	
	var var_name = $("#txt_food_name").val();
	if(var_name == ""){
		layer.alert("请先输入信息!",
				{
					title:"提示",
					icon:6
				});
		return false;
	}
	return true;
}
function check_logout(){
	layer.confirm("确定要注销吗?",
			{
				title:"提示",
				icon:3
			},function(){
				$.session.clear();
				location.href="logout";
			});
}
function a_add_to_car(foodId){
	if($.session.get("is_login") != null){
		layer.confirm("确定要加入购物车吗?",{
			title:"提示",
			icon:3
		},
		function(){
			var wait = layer.load(3);
			$.post("addToShopCar",
					{"foodId":foodId},
					function(data){
						layer.close(wait);
						if(data == "OK"){
							
							layer.alert("已加入购物车!",
									{
										title:"提示",
										icon:6,
										closeBtn:0
										
									});
						}else{
							layer.alert("操作失败!请重新登录!",
									{
										title:"提示",
										icon:5,
										closeBtn:0
									},function(){
										location.href="user/login.jsp";
									});
						}
					}
					);
		});
	}else{
		layer.alert("您还没登录,请先登录!",
				{
					title:"提示",
					icon:5,
					closeBtn:0
				},function(){
					location.href="user/login.jsp";
				});
	}
	
}


function modify_food(foodId){
	location.href="admin/preEditFood?food.foodId="+foodId;
}
function delete_food(foodId){
	layer.confirm("确定要删除吗?",
			{
				title:"提示",
				icon:3
			},
			function(){
				var wait = layer.load(3);
				$.post("admin/deleteFood",
						{
							"food.foodId":foodId
						},
						function(data){
							layer.close(wait);
							if(data == "OK"){
								layer.alert("删除成功!",{
									title:"提示",
									icon:6,
									closeBtn:0
								},function(){
									location.href="admin/index"; 
								});
							}else{
								layer.alert("删除餐品失败!可能被用户订单引用!",
										{
											title:"提示",
											icon:5
										});
							}
						});
			});
}
function admin_login_out(){
	layer.confirm("确定要注销吗?",
			{
				title:"提示",
				icon:3
			},function(){
				location.href="admin/logout";
			});
}