
function a_clear_car(){
	layer.confirm("确定要清空购物车吗?",
			{
				title:"提示",
				icon:3
			},
			function(){
				var wait = layer.load(3);
				$.post("clearShopCar",
						function(data){
					layer.close(wait);
					if(data == "OK"){
						layer.alert("购物车已清空!",
								{
									title:"提示",
									icon:6,
									closeBtn:0
								},function(){
									location.href="showShopCar";
								});
					}else{
						layer.alert("操作失败!");
					}
				});
			});
}
function btn_delete(foodId){
	layer.confirm("确定要从购物车移除吗?",
			{
				title:"提示",
				icon:3
			},function(){
				var wait = layer.load(3);
				$.post("deleteShopCarItem",
						{
							foodId:foodId
						},function(data){
							layer.close(wait);
							if(data == "OK"){
								layer.alert("已移除!",
										{
											title:"提示",
											icon:6,
											closeBtn:0
										},function(){
											location.href="showShopCar";
										});
							}else{
								layer.alert("操作失败!");
							}
						});
			});
}
function a_make_order(){
	var wait = layer.load(3);
	$.post("makeOrders",
			function(data){
		layer.close(wait);
		if(data == "OK"){
			layer.alert("订单已生成!",
					{
						title:"提示",
						icon:6,
						closeBtn:0
					},function(){
						location.href="index";
					});
		}else{
			layer.alert(data);
		}
		
	});
}

function btn_show_order(orderId){
	location.href="showOrdersDetail?orderId="+orderId;
}

function modify_quantity(value,ob){
	
	if(value == ""){
		layer.alert("请先输入数量!");
		$(ob).val(1);
		$(ob).next().hide();
		return;
	}
	if(value == 0){
		layer.alert("数量不能为0!")
		$(ob).val(1);
		$(ob).next().hide();
		$(ob).next().hide();
		return;
	}
	var z = /^[1-9]*$/;
	if(!z.test(value)){
		layer.alert("必须输入整数!")
		$(ob).val(1);
		$(ob).next().hide();
		return;
	}
	
	
}
function btn_modify(foodId,ob){
	
		
		$.post("modifyQuantity",
				{
					quantity:$(ob).prev().val(),
					foodId:foodId
				},function(data){
					if(data == "OK"){
						location.href="showShopCar";
					}else{
						layer.alert("修改失败!");
						$(ob).hide();
					}
				}
			);
}
function click_switch(){
	var name = $("#txt_switch option:selected").val();
	if(name == "order_id"){
		$("#txt_order_id").show();
		$("#txt_user_name").hide();
	}else if(name == "user_name"){
		$("#txt_order_id").hide();
		$("#txt_user_name").show();
	}else{
		$("#txt_order_id").hide();
		$("#txt_user_name").hide();
	}
}
function btn_order_search(){
	var var_select = $("#txt_switch option:selected").val();
	var state = $("#txt_order_state option:selected").val();
	if(var_select == "order_id"){
		var order_id = $("#txt_order_id").val();
		
		if($.trim(order_id) == ''){
			layer.alert("订单号不能为空!",
					{
						title:"提示",
						icon:5
					});
			return;
		}
		var reg = /^\d+$/;
		if(!reg.test(order_id)){
			layer.alert("订单号不合法!",
					{
						title:"提示",
						icon:5
					});
			return;
		}
		location.href="admin/searchOrder?orderId="+order_id+"&orderState="+state;
		return;
	}else if(var_select == "user_name"){
		var user_name = $("#txt_user_name").val();
		if($.trim(user_name) == ''){
			layer.alert("用户名不能为空!",
					{
						title:"提示",
						icon:5
					});
			return;
		}
		location.href="admin/searchOrder?userName="+user_name+"&orderState="+state;
		return;
	}else{
		location.href="admin/searchOrder?orderState="+state;
	}
	
}
function btn_order_do(orderId){
	var wait = layer.load(3);
	$.post("admin/doOrder",
			{
				orderId:orderId,
				orderState:"已发货"
			},function(data){
				layer.close(wait);
				if(data == "OK"){
					layer.alert("发货成功!",
							{
								title:"提示",
								icon:6,
								closeBtn:0
							},function(){
								 location.reload();
							});
				}else{
					layer.alert(data);
				}
			});
}
function btn_order_back(orderId){
	var wait = layer.load(3);
	$.post("admin/doOrder",
			{
				orderId:orderId,
				orderState:"未处理"
			},function(data){
				layer.close(wait);
				if(data == "OK"){
					layer.alert("取消发货成功!",
							{
								title:"提示",
								icon:6,
								closeBtn:0
							},function(){
								 location.reload();
							});
				}else{
					layer.alert("操作失败!");
				}
			});
}
