package com.lqq.test;




import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.jdbc.Work;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.lqq.entity.FoodTypes;
import com.lqq.entity.Foods;
import com.lqq.entity.Orders;
import com.lqq.entity.Users;


public class MyTest {

	ApplicationContext applicationContext;
	SessionFactory sessionFactory;
	Session session;
	Transaction transaction;
	@Before
	public void Init(){
		 applicationContext = new ClassPathXmlApplicationContext("mytest.xml");
		 sessionFactory = (SessionFactory) applicationContext.getBean("sessionFactory");
		 session = sessionFactory.getCurrentSession();
		 transaction = session.beginTransaction();
		 
	}
	
	@Test
	public void Test(){
		Foods foods = (Foods) session.get(Foods.class, 2);
		Foods foods2 = (Foods) session.get(Foods.class, 3);
		transaction.commit();
		
		session = sessionFactory.getCurrentSession();
		transaction = session.beginTransaction();
		Set<FoodTypes> types = foods.getFoodTypes();
		//FoodTypes types = (FoodTypes) session.get(FoodTypes.class, 1);
		for(FoodTypes types2 : types){
			types2.getFoods().add(foods2);
			session.update(types2);
		}
	}
	//添加
	@Test
	public void testAdd()  {
		
		Foods foods1 = new Foods();
		foods1.setFoodDescription("很好吃!");
		foods1.setFoodName("food-a");
		foods1.setFoodPrice(100.0F);
		foods1.setFoodSummary("好吃的很!");
		Foods foods2 = new Foods();
		foods2.setFoodDescription("很好吃!");
		foods2.setFoodName("food-b");
		foods2.setFoodPrice(100.0F);
		foods2.setFoodSummary("好吃的很!");
		
		FoodTypes foodTypes1 = new FoodTypes();
		foodTypes1.setFoodTypeName("种类-1");
		FoodTypes foodTypes2 = new FoodTypes();
		foodTypes2.setFoodTypeName("种类-2");
		
		foodTypes1.getFoods().add(foods1);
		foodTypes1.getFoods().add(foods2);
		foodTypes2.getFoods().add(foods1);
		foodTypes2.getFoods().add(foods2);
		
		session.save(foods1);
		session.save(foods2);
		session.save(foodTypes1);
		session.save(foodTypes2);
		
		
		
//		Users users = new Users();
//		users.setAddress("安徽省");
//		users.setEmail("741830313@qq.com");
//		users.setLoginName("lqq");
//		users.setLoginPassword("123");
//		users.setPhone("18205659052");
//		users.setRealName("罗强强");
//		
//	    Foods foods = (Foods) session.get(Foods.class, 1);
//		Orders orders = new Orders();
//		orders.setFoodCount(3);
//		orders.setFoods(foods);
//		orders.setOrderPrice(foods.getFoodPrice()*3);
//		orders.setOrderState("未处理");
//		orders.setUsers(users);
//		orders.setOrderTime(new Date());
//		
//		 Foods food2 = (Foods) session.get(Foods.class, 1);
//			Orders orders2 = new Orders();
//			orders2.setFoodCount(3);
//			orders2.setFoods(food2);
//			orders2.setOrderPrice(food2.getFoodPrice()*3);
//			orders2.setOrderState("未处理");
//			orders2.setUsers(users);
//			orders2.setOrderTime(new Date());
//		
//		session.save(users);
//		session.save(orders);
//		session.save(orders2);
		
	}
	//更新
	@Test
	public void testUpdate(){
//		Admins admins = (Admins) session.get(Admins.class, 1);
//		admins.setLoginName("lqq");
//		session.update(admins);
		
		
		
	}
	//删除
	@Test
	public void testDelete(){
		
	}
	
	@Test
	public void testGet1(){
		
	}
	//查询
	@Test
	public void testGet(){
		
		session.doWork(new Work() {
			
			@Override
			public void execute(Connection arg0) throws SQLException {
				// TODO Auto-generated method stub
				String sql = "SELECT f.FOOD_NAME from FOODS as f, FOOD_FOOD_TYPE as t"
						+ " WHERE t.FOOD_ID=f.FOOD_ID AND t.FOOD_TYPE_ID=1 ORDER BY f.FOOD_ID DESC LIMIT 0,4";
						
				Statement statement = arg0.createStatement();
				ResultSet rs = statement.executeQuery(sql);
				while(rs.next()){
					System.out.println(rs.getString(1));
				}
			}
		});
		
//		FoodTypes foodTypes = (FoodTypes) session.get(FoodTypes.class, 1);
//		System.out.println(foodTypes.getFoods().size());
//		
	}
	
	@After
	public void destory(){
		transaction.commit();
		((ClassPathXmlApplicationContext)applicationContext).close();
	}

}
