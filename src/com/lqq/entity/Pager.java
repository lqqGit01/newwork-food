package com.lqq.entity;

public class Pager {
	
	private int curPage; //当前页
	private int pageSize; //每页显示的数量
	private int rowCount; //总行数
			int pageCount; //页数统计
			public int getCurPage() {
				return curPage;
			}
			public void setCurPage(int curPage) {
				this.curPage = curPage;
			}
			public int getPageSize() {
				return pageSize;
			}
			public void setPageSize(int pageSize) {
				this.pageSize = pageSize;
			}
			public int getRowCount() {
				return rowCount;
			}
			public void setRowCount(int rowCount) {
				this.rowCount = rowCount;
			}
			public int getPageCount() {
				return (rowCount %2 == 0) ? (rowCount / pageSize) : (rowCount / pageSize + 1);
			}
	
	

}
