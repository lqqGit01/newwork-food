package com.lqq.entity;

public class OrdersAttach {
	
	private Integer ordersAttachId;
	private Foods foods;
	private Integer foodCount;
	private float orderPrice;
	private Orders orders;
	
	public Orders getOrders() {
		return orders;
	}
	public void setOrders(Orders orders) {
		this.orders = orders;
	}
	public Integer getOrdersAttachId() {
		return ordersAttachId;
	}
	public void setOrdersAttachId(Integer ordersAttachId) {
		this.ordersAttachId = ordersAttachId;
	}
	public Foods getFoods() {
		return foods;
	}
	public void setFoods(Foods foods) {
		this.foods = foods;
	}
	public Integer getFoodCount() {
		return foodCount;
	}
	public void setFoodCount(Integer foodCount) {
		this.foodCount = foodCount;
	}
	public float getOrderPrice() {
		return orderPrice;
	}
	public void setOrderPrice(float orderPrice) {
		this.orderPrice = orderPrice;
	}

}
