package com.lqq.entity;

public class CarItem{
	
	private Foods foods;
	private int quantity;
	public Foods getFoods() {
		return foods;
	}
	public void setFoods(Foods foods) {
		this.foods = foods;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public CarItem(Foods foods, int quantity) {
		super();
		this.foods = foods;
		this.quantity = quantity;
	}
	public CarItem() {
		// TODO Auto-generated constructor stub
	}

}
