package com.lqq.action;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;
import com.lqq.biz.OrderBiz;
import com.lqq.biz.OrderMainBiz;
import com.lqq.biz.UserBiz;
import com.lqq.entity.CarItem;
import com.lqq.entity.Orders;
import com.lqq.entity.OrdersAttach;
import com.lqq.entity.Users;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class UserAction extends ActionSupport implements RequestAware,SessionAware,ServletResponseAware{
	
	private Users user;
	public Users getUser() {
		return user;
	}
	public void setUser(Users user) {
		this.user = user;
	}
	
	UserBiz userBiz;
	public void setUserBiz(UserBiz userBiz) {
		this.userBiz = userBiz;
	}

	//登录
	public String login(){
		if(user.getLoginName() == null ||user.getLoginName().equals("") || user.getLoginPassword() == null || user.getLoginPassword().equals("")){
			addActionError("用户名或密码不能为空!");
			return INPUT;
		}
		if(userBiz.checkLogin(user)){
			user = userBiz.getUserByLoginName(user.getLoginName());
			sessionMap.put("user", user);
			return SUCCESS;
			
		}else{
			addActionError("用户名或密码错误!");
			return INPUT;
		}
	}
	//弹窗登录
	public String layerLogin() throws IOException{
		
		if(userBiz.checkLogin(user)){
			user = userBiz.getUserByLoginName(user.getLoginName());
			sessionMap.put("user", user);
			response.getWriter().write("OK");
			return null;
			
		}else{
			response.getWriter().write("ERROR_USER");
			return null;
		}
		
	}
	
	public String test() throws Exception{
		sessionMap.put("name", "lqq");
		return SUCCESS;
	}
	
	//修改个人信息
	public String modifyUser(){
		user = (Users) sessionMap.get("user");
		return SUCCESS;
	}
	
	public String doModifyUser() throws Exception{
		if(userBiz.updataUserinfo(user.getUserId(),user.getLoginName(), user.getRealName(), user.getEmail(), user.getPhone(), user.getAddress())){
			response.getWriter().write("OK");
			return null;
		}else{
			response.getWriter().write("FALSE");
			return null;
		}
	}
	
	public void clearSession(){
		sessionMap.clear();
	}
	//登录界面索引
	public String loginIndex(){
		clearSession();
		return SUCCESS;
	}
	//注销
	public String logout(){
		clearSession();
		return SUCCESS;
	}
	
	OrderBiz orderBiz;
	OrderMainBiz orderMainBiz;
	
	public void setOrderMainBiz(OrderMainBiz orderMainBiz) {
		this.orderMainBiz = orderMainBiz;
	}
	public void setOrderBiz(OrderBiz orderBiz) {
		this.orderBiz = orderBiz;
	}
	//生成订单
	@SuppressWarnings("unchecked")
	public String makeOrders() {
		
		Orders orders = new Orders();
		orders.setOrderState("未处理");
		orders.setOrderTime(new Date());
		user = (Users) sessionMap.get("user");
		orders.setUsers(user);
		orderMainBiz.addOrderMain(orders);
		
		Map<Integer, Object> car = (Map<Integer, Object>) sessionMap.get("car");
		Iterator<Integer> iterator = car.keySet().iterator();
		while(iterator.hasNext()){
			Integer key = iterator.next();
			CarItem item = (CarItem) car.get(key);
			OrdersAttach ordersAttach = new OrdersAttach();
			ordersAttach.setFoodCount(item.getQuantity());
			ordersAttach.setFoods(item.getFoods());
			ordersAttach.setOrderPrice(item.getQuantity() * item.getFoods().getFoodPrice());
			ordersAttach.setOrders(orders);
			orderBiz.addOrder(ordersAttach);
		}
		sessionMap.remove("car");
		try {
			response.getWriter().write("OK");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	}
	//查看主订单
	public String showOrders(){
		
		user = (Users) sessionMap.get("user");
		Set<Orders> list = orderMainBiz.getOrdersByUserId(user.getUserId());
		request.put("orderMainList", list);
		return SUCCESS;
	}
	
	//用于接受orderId
	private Integer orderId;
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	public String showOrdersDetail(){
		
		Set<OrdersAttach> ordersAttachs = orderBiz.getOrdersDetailByOrderId(orderId);
		request.put("orderDetailList", ordersAttachs);
		return SUCCESS;
	}
	
	//注册用户
	public String addUser() throws IOException{
		
		if(userBiz.loginNameIsExist(user.getLoginName())){
			response.getWriter().write("FALSE");
			return null;
		}
		userBiz.addUser(user);
		response.getWriter().write("OK");
		
		return null;
	}

	Map<String, Object> request;
	@Override
	public void setRequest(Map<String, Object> arg0) {
		// TODO Auto-generated method stub
		
		this.request = arg0;
	}
	
	Map<String, Object> sessionMap;
	@Override
	public void setSession(Map<String, Object> arg0) {
		// TODO Auto-generated method stub
		
		this.sessionMap = arg0;
		
	}
	HttpServletResponse response;
	@Override
	public void setServletResponse(HttpServletResponse arg0) {
		// TODO Auto-generated method stub
		this.response = arg0;
	}

}
