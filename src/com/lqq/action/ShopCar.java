package com.lqq.action;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;

import com.lqq.biz.FoodBiz;
import com.lqq.entity.CarItem;
import com.lqq.entity.Foods;
import com.opensymphony.xwork2.ActionSupport;

public class ShopCar extends ActionSupport implements SessionAware,ServletResponseAware{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int foodId;
	private int quantity;
	
	public int getFoodId() {
		return foodId;
	}
	public void setFoodId(int foodId) {
		this.foodId = foodId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	FoodBiz foodBiz;
	
	public void setFoodBiz(FoodBiz foodBiz) {
		this.foodBiz = foodBiz;
	}
	
	@SuppressWarnings("unchecked")
	public String addToShopCar(){
		
		
		//获取购物车
		Map<Integer,Object> car = (Map<Integer, Object>) sessionMap.get("car");
		
		Foods foods = foodBiz.getFoodById(foodId);
		if(car == null){
			car = new HashMap<Integer,Object>();
			sessionMap.put("car", car);
		}
		
		//检查购物车
		CarItem item = (CarItem) car.get(foods.getFoodId());
		if(item != null){
			item.setQuantity(item.getQuantity() + 1);
			car.put(foodId, item);
			
		}else{
			
			car.put(foodId, new CarItem(foods,1));
			
		}
		try {
			response.getWriter().write("OK");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	//修改数量
	@SuppressWarnings("unchecked")
	public String modifyQuantity() throws IOException{
		
		Map<Integer,Object> car = (Map<Integer, Object>) sessionMap.get("car");
		CarItem item = (CarItem) car.get(foodId);
		if(quantity <= 0 ){
			response.getWriter().write("FALSE");
			return null;
		}
		item.setQuantity(quantity);
		car.put(foodId, item);
		sessionMap.put("car", car);
		response.getWriter().write("OK");
		return null;
		
	}
	
	//查看购物车
	public String showShopCar(){
		return SUCCESS;
	}
	//清空购物车
	public String clearShopCar() throws IOException{
		
		sessionMap.remove("car");
		response.getWriter().write("OK");
		return null;
	}
	//删除购物车餐品
	@SuppressWarnings("unchecked")
	public String deleteShopCarItem() throws IOException{
		Map<Integer, Object> car = (Map<Integer, Object>) sessionMap.get("car");
		car.remove(foodId);
		sessionMap.put("car", car);
		response.getWriter().write("OK");
		
		return null;
	}

	Map<String, Object> sessionMap;
	@Override
	public void setSession(Map<String, Object> arg0) {
		// TODO Auto-generated method stub
		this.sessionMap = arg0;
	}
	
	HttpServletResponse response;
	@Override
	public void setServletResponse(HttpServletResponse arg0) {
		// TODO Auto-generated method stub
		this.response = arg0;
	}
	
	

}
