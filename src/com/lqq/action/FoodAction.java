package com.lqq.action;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;
import com.lqq.biz.FoodBiz;
import com.lqq.biz.FoodTypeBiz;
import com.lqq.entity.FoodTypes;
import com.lqq.entity.Foods;
import com.lqq.entity.Pager;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class FoodAction extends ActionSupport implements RequestAware,ServletResponseAware,SessionAware{
	
	
	private Foods foods;
	private FoodTypes foodTypes;
	private Pager pager;
	private Integer foodTypeId;
	
	
	public Integer getFoodTypeId() {
		return foodTypeId;
	}

	public void setFoodTypeId(Integer foodTypeId) {
		this.foodTypeId = foodTypeId;
	}

	public Pager getPager() {
		return pager;
	}

	public void setPager(Pager pager) {
		this.pager = pager;
	}

	FoodBiz foodBiz;
	public void setFoodBiz(FoodBiz foodBiz) {
		this.foodBiz = foodBiz;
	}

	public Foods getFoods() {
		return foods;
	}

	public void setFoods(Foods foods) {
		this.foods = foods;
	}

	public FoodTypes getFoodTypes() {
		return foodTypes;
	}

	public void setFoodTypes(FoodTypes foodTypes) {
		this.foodTypes = foodTypes;
	}

	//首页
	public String index(){
		
		int curPage = 1;
		int pageSize = 6;
		
		if(pager != null){
			curPage = pager.getCurPage();
		}
		List<Foods> foodsList = null;
		
		if(foodTypeId != null){
			
			foodsList = foodBiz.getFoodsByPagerAndType(foodTypeId, curPage, pageSize);
			pager = foodBiz.getPagerByFoodType(foodTypeId, pageSize);
		}else{
			foodsList = foodBiz.getFoodsByPager(curPage, pageSize);
			pager = foodBiz.getFoodPager(pageSize);
		}
		pager.setCurPage(curPage);
		request.put("foodList", foodsList);
		getFoodTypesList();
		return SUCCESS;
	}
	
	
	
	private String foodName;
	
	public String getFoodName() {
		return foodName;
	}

	public void setFoodName(String foodName) {
		this.foodName = foodName;
	}

	//按菜名搜索
	public String search(){
		
		int curPage = 1;
		int pageSize = 6;
		if(pager != null){
			curPage = pager.getCurPage();
		}
		List<Foods> foodList = foodBiz.getFoodsByPagerAndName(foodName, curPage, pageSize);
		pager = foodBiz.getPagerByFoodName(foodName, pageSize);
		pager.setCurPage(curPage);
		
		request.put("foodList", foodList);
		getFoodTypesList();
		return SUCCESS;
	}
	
	//餐品详细信息
	public String detail(){
		foods = foodBiz.getFoodById(foods.getFoodId());
		getFoodTypesList();
		return SUCCESS;
	}
	
	FoodTypeBiz foodTypeBiz;
	
	public void setFoodTypeBiz(FoodTypeBiz foodTypeBiz) {
		this.foodTypeBiz = foodTypeBiz;
	}

	//获取餐品类别列表
	public void getFoodTypesList(){
		List<FoodTypes> list = foodTypeBiz.getAllFoodTypes();
		request.put("foodTypeList", list);
	}
	
	
	//查看购物车
	public String shopCar(){
		
		return SUCCESS;
	}

	Map<String, Object> request;
	@Override
	public void setRequest(Map<String, Object> arg0) {
		// TODO Auto-generated method stub
		this.request = arg0;
	}

	
	HttpServletResponse response;
	@Override
	public void setServletResponse(HttpServletResponse arg0) {
		// TODO Auto-generated method stub
		this.response = arg0;
	}

	Map<String, Object> sessionMap;
	@Override
	public void setSession(Map<String, Object> arg0) {
		// TODO Auto-generated method stub
		this.sessionMap = arg0;
		
	}

}
