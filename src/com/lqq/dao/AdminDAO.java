package com.lqq.dao;

import com.lqq.entity.Admins;

public interface AdminDAO {
	
	//检查用户信息是否正确
	public boolean check_login(Admins admins);
	
	//检查用户名是否存在
	public boolean loginNameIsExist(String loginName);
	
	//修改用户信息
	public boolean updateAdmin(Admins admins);
	
	//添加管理员
	public void add(Admins admins);
	

}
