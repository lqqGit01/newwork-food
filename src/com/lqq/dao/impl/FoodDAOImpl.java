package com.lqq.dao.impl;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.jdbc.Work;
import com.lqq.dao.FoodDAO;
import com.lqq.entity.FoodTypes;
import com.lqq.entity.Foods;

public class FoodDAOImpl implements FoodDAO {

	SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Foods> getFoodsByPager(int curPage, int pageSize) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Foods.class);
		criteria.addOrder(Order.asc("foodId"));
		criteria.setFirstResult((curPage - 1)*pageSize);
		criteria.setMaxResults(pageSize);
		return criteria.list();
	}

	@Override
	public Integer getAllFoodsCount() {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Foods.class);
		return criteria.list().size();
	}

	@Override
	public List<Foods> getFoodsByPagerAndType(Integer foodTypeId, int curPage, int pageSize) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		List<Foods> foodList = new ArrayList<Foods>();
		session.doWork(new Work() {
			
			@Override
			public void execute(Connection arg0) throws SQLException {
				// TODO Auto-generated method stub
				String sql = "SELECT f.* FROM FOODS f,FOOD_FOOD_TYPE t"
						+ " WHERE f.FOOD_ID=t.FOOD_ID AND t.FOOD_TYPE_ID="+foodTypeId+""
								+ " ORDER BY f.FOOD_ID DESC"
								+ " LIMIT "+((curPage-1)*pageSize)+","+pageSize;
									
				Statement statement = arg0.createStatement();
				ResultSet rs = statement.executeQuery(sql);
				
				while(rs.next()){
					Integer id = rs.getInt(1);
					String name = rs.getString(2);
					String summary = rs.getString(3);
					String description = rs.getString(4);
					float price = rs.getFloat(5);
					String image = rs.getString(6);
					Foods foods = new Foods(id, name, summary, description, price, image);
					foodList.add(foods);
				}
			}
		});
		return foodList;
	}

	@Override
	public Integer getAllFoodsCountByType(Integer foodTypeId) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		FoodTypes foodTypes = (FoodTypes) session.get(FoodTypes.class, foodTypeId);
		return foodTypes.getFoods().size();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Foods> getFoodsByPagerAndName(String foodName, int curPage, int pageSize) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Foods.class);
		criteria.add(Restrictions.like("foodName", foodName,MatchMode.ANYWHERE));
		criteria.addOrder(Order.desc("foodId"));
		criteria.setFirstResult((curPage-1)*pageSize);
		criteria.setMaxResults(pageSize);
		
		return criteria.list();
	}

	@Override
	public Integer getAllFoodsCountByName(String foodName) {
		// TODO Auto-generated method stub
		
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Foods.class);
		criteria.add(Restrictions.like("foodName", foodName,MatchMode.ANYWHERE));
		
		return criteria.list().size();
	}

	@Override
	public Foods getFoodById(Integer foodId) {
		// TODO Auto-generated method stub
		
		Session session = sessionFactory.getCurrentSession();
		Foods foods = (Foods) session.get(Foods.class, foodId);
		return foods;
	}

	@Override
	public void add(Foods foods) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.save(foods);
	}

	@Override
	public void update(Foods foods) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.update(foods);
	}

	@Override
	public void delete(Foods foods) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.delete(foods);
	}


	

}
