package com.lqq.dao.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.jdbc.Work;

import com.lqq.dao.FoodAndFoodTypeDAO;

public class FoodAndFoodTypeDAOImpl implements FoodAndFoodTypeDAO {

	SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void deleteByFoodIdAndFoodTypeId(Integer foodId, Integer foodTypeId) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.doWork(new Work() {
				@Override
				public void execute(Connection connection) throws SQLException {
					// TODO Auto-generated method stub
					
					String sql = "DELETE FROM food_food_type WHERE FOOD_ID="+foodId+" AND FOOD_TYPE_ID="+foodTypeId;
					Statement statement = connection.createStatement();
					statement.executeUpdate(sql);
				}
			});
			
		
		
		
	}
	

}
