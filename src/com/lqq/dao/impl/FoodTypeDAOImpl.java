package com.lqq.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.lqq.dao.FoodTypeDAO;
import com.lqq.entity.FoodTypes;

public class FoodTypeDAOImpl implements FoodTypeDAO {

	SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FoodTypes> getAllFoodTypes() {
		// TODO Auto-generated method stub
		
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(FoodTypes.class);
		return criteria.list();
	}

	@Override
	public FoodTypes getFoodTypeByFoodTypeId(Integer id) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		
		return (FoodTypes) session.get(FoodTypes.class, id);
	}

	@Override
	public void update(FoodTypes foodTypes) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.update(foodTypes);
	}

}
