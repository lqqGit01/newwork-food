package com.lqq.dao.impl;


import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.lqq.dao.OrderDAO;
import com.lqq.entity.Orders;
import com.lqq.entity.OrdersAttach;

public class OrderDAOImpl implements OrderDAO {
	
	SessionFactory sessionFactory;
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void addOrder(OrdersAttach ordersAttach) {
		// TODO Auto-generated method stub
		try {
			Session session = sessionFactory.getCurrentSession();
			session.save(ordersAttach);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}

	@Override
	public Set<OrdersAttach> getOrdersDetailByOrderId(Integer orderId) {
		// TODO Auto-generated method stub
		
		Session session = sessionFactory.getCurrentSession();
		Orders orders = (Orders) session.get(Orders.class, orderId);
		Set<OrdersAttach> ordersAttachs = orders.getOrdersAttachs();
		return ordersAttachs;
	}


}
