package com.lqq.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

import com.lqq.dao.AdminDAO;
import com.lqq.entity.Admins;


public class AdminDAOImpl implements AdminDAO {

	SessionFactory sessionFactory;
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public boolean check_login(Admins admins) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Admins.class);
		Example example = Example.create(admins);
		criteria.add(example);
		if(criteria.list().size() > 0){
			return true;
		}
		return false;
	}

	@Override
	public boolean loginNameIsExist(String loginName) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from ADMINS where loginName='"+loginName+"'");
		if(query.list().size() > 0){
			return true;
		}
		return false;
	}

	@Override
	public boolean updateAdmin(Admins admins) {
		// TODO Auto-generated method stub
		
		Session session = sessionFactory.getCurrentSession();
		try {
			session.update(admins);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public void add(Admins admins) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.save(admins);
	}

}
