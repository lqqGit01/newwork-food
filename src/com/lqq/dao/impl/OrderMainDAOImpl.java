package com.lqq.dao.impl;


import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.lqq.dao.OrderMainDAO;
import com.lqq.entity.Orders;
import com.lqq.entity.Users;

public class OrderMainDAOImpl implements OrderMainDAO {

	SessionFactory sessionFactory;
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void addOrderMain(Orders orders) {
		// TODO Auto-generated method stub

		Session session = sessionFactory.getCurrentSession();
		session.save(orders);
	}

	
	@Override
	public Set<Orders> getOrdersByUserId(Integer userId) {
		// TODO Auto-generated method stub

		Session session = sessionFactory.getCurrentSession();
		Users user = (Users) session.get(Users.class, userId);
		
		return user.getOrderses();
	}

	@Override
	public void update(Orders orders) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.update(orders);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Orders> getAllOrdersByPager(int curPage, int pageSize) {
		// TODO Auto-generated method stub
		
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Orders.class);
		criteria.setFirstResult((curPage - 1)*pageSize);
		criteria.setMaxResults(pageSize);
		criteria.addOrder(Order.desc("orderTime"));
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Orders> getOrderByOrderIdAndState(int curPage,int pageSize,Integer orderId,String orderState) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Orders.class);
		if(orderId != null){
			criteria.add(Restrictions.eq("orderId", orderId));
		}
		
		if(orderState.equals("no")){
			criteria.add(Restrictions.eq("orderState", "未处理"));
		}else if(orderState.equals("yes")){
			criteria.add(Restrictions.eq("orderState", "已发货"));
		}
		criteria.setFirstResult((curPage - 1)*pageSize);
		criteria.setMaxResults(pageSize);
		criteria.addOrder(Order.desc("orderTime"));
		return criteria.list();
	}

	@Override
	public Integer getCountAllOrders() {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Orders.class);
		
		return criteria.list().size();
	}

	@Override
	public Orders getOrderByOrderId(Integer orderId) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		return (Orders) session.get(Orders.class, orderId);
	}


	@Override
	public Integer getCountOfOrdersByOrderIdAndState(Integer orderId, String orderState) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Orders.class);
		if(orderId != null){
			criteria.add(Restrictions.eq("orderId", orderId));
		}
		if(orderState.equals("no")){
			criteria.add(Restrictions.eq("orderState", "未处理"));
		}else if(orderState.equals("yes")){
			criteria.add(Restrictions.eq("orderState", "已发货"));
		}
		return criteria.list().size();
	}

	@Override
	public Set<Orders> getOrdersByUserAndOrderState(String userName,String orderState) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM Users WHERE loginName='"+userName+"'");
		Users users = (Users) query.uniqueResult();
		Set<Orders> list = new HashSet<>();
		if(users != null){
			if(orderState.equals("no")){
				for(Orders orders : users.getOrderses()){
					if(orders.getOrderState().equals("未处理")){
						list.add(orders);
					}
				}
			}else if(orderState.equals("yes")){
				for(Orders orders : users.getOrderses()){
					if(orders.getOrderState().equals("已发货")){
						list.add(orders);
					}
				}
			}else{
				list = users.getOrderses();
			}
		}
		return list;
	}

	@Override
	public Integer getCountOfOrdersByUserAndOrderState(String userName, String orderState) {
		// TODO Auto-generated method stub
		
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM Users WHERE loginName='"+userName+"'");
		Users users = (Users) query.uniqueResult();
		Set<Orders> list = new HashSet<>();
		if(users != null){
			if(orderState.equals("no")){
				for(Orders orders : users.getOrderses()){
					if(orders.getOrderState().equals("未处理")){
						list.add(orders);
					}
				}
			}else if(orderState.equals("yes")){
				for(Orders orders : users.getOrderses()){
					if(orders.getOrderState().equals("已发货")){
						list.add(orders);
					}
				}
			}else{
				list = users.getOrderses();
			}
		}
		return list.size();
	}
}
