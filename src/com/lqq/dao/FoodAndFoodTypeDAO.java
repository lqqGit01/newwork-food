package com.lqq.dao;

public interface FoodAndFoodTypeDAO {
	
	//通过foodId删除对应数据
	public void deleteByFoodIdAndFoodTypeId(Integer foodId,Integer foodTypeId);

}
