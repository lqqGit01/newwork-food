package com.lqq.dao;


import java.util.Set;

import com.lqq.entity.OrdersAttach;

public interface OrderDAO {
	
	//生成订单
	public void addOrder(OrdersAttach ordersAttach);
	//查看明细订单
	public Set<OrdersAttach> getOrdersDetailByOrderId(Integer orderId);
	//订单处理

}
