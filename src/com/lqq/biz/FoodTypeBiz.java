package com.lqq.biz;

import java.util.List;

import com.lqq.entity.FoodTypes;

public interface FoodTypeBiz {
		//获取所有菜系
		public List<FoodTypes> getAllFoodTypes();
		//根据Id获取菜系
		public FoodTypes getFoodTypeByFoodTypeId(Integer id);
		//更新
		public void update(FoodTypes foodTypes);
}
