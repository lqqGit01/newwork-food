package com.lqq.biz;

import java.util.Set;
import com.lqq.entity.Orders;
import com.lqq.entity.Users;

public interface UserBiz {
		//登录检查
		public boolean checkLogin(Users users);
		//检查用户是否存在
		public boolean loginNameIsExist(String loginName);
		//获取用户订单信息
		public Set<Orders> getAllOrders(Integer userId);
		//通过登录名获取用户
		public Users getUserByLoginName(String loginName);
		//修改个人信息
		public boolean updataUserinfo(Integer userId,String loginName,String realName,String email,String phone,String address);
		//注册用户
		public void addUser(Users user);


}
