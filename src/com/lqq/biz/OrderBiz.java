package com.lqq.biz;


import java.util.Set;

import com.lqq.entity.OrdersAttach;

public interface OrderBiz {
	
		//生成订单
		public void addOrder(OrdersAttach ordersAttach);
		//查看明细订单
		public Set<OrdersAttach> getOrdersDetailByOrderId(Integer orderId);
}
