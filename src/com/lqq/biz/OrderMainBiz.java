package com.lqq.biz;

import java.util.List;
import java.util.Set;

import com.lqq.entity.Orders;
import com.lqq.entity.Pager;

public interface OrderMainBiz {
	
		//生成主订单
		public void addOrderMain(Orders orders);
		//查看订单
		public Set<Orders> getOrdersByUserId(Integer userId);
		//订单处理
		public void update(Orders orders);
		
		//获取全部订单
		public List<Orders> getAllOrdersByPager(int curPage,int pageSize);
		public Pager getPager(int pageSize);
		//根据订单id和订单状态,用户登录名获取订单
		public List<Orders> getOrderByOrderIdAndState(int curPage,int pageSize,Integer orderId,String orderState);
		public Pager getPagerByOrderIdAndState(int pageSize,Integer orderId,String orderState);
		//根据订单id获取订单
		public Orders getOrderByOrderId(Integer orderId);
		//根据用户信息获取订单列表
		public Set<Orders> getOrdersByUserAndOrderState(String userName,String orderState);
		public Integer getCountOfOrdersByUserAndOrderState(String userName,String orderState);
}
