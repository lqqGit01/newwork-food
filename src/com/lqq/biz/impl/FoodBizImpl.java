package com.lqq.biz.impl;

import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import com.lqq.biz.FoodBiz;
import com.lqq.dao.FoodDAO;
import com.lqq.entity.Foods;
import com.lqq.entity.Pager;

@Transactional
public class FoodBizImpl implements FoodBiz {

	FoodDAO foodDAO;
	
	public void setFoodDAO(FoodDAO foodDAO) {
		this.foodDAO = foodDAO;
	}

	@Override
	public List<Foods> getFoodsByPager(int curPage, int pageSize) {
		// TODO Auto-generated method stub
		return foodDAO.getFoodsByPager(curPage, pageSize);
	}

	@Override
	public Integer getAllFoodsCount() {
		// TODO Auto-generated method stub
		return foodDAO.getAllFoodsCount();
	}

	@Override
	public List<Foods> getFoodsByPagerAndType(Integer foodTypeId, int curPage, int pageSize) {
		// TODO Auto-generated method stub
		return foodDAO.getFoodsByPagerAndType(foodTypeId, curPage, pageSize);
	}

	@Override
	public Integer getAllFoodsCountByType(Integer foodTypeId) {
		// TODO Auto-generated method stub
		return foodDAO.getAllFoodsCountByType(foodTypeId);
	}

	@Override
	public List<Foods> getFoodsByPagerAndName(String foodName, int curPage, int pageSize) {
		// TODO Auto-generated method stub
		return foodDAO.getFoodsByPagerAndName(foodName, curPage, pageSize);
	}

	@Override
	public Integer getAllFoodsCountByName(String foodName) {
		// TODO Auto-generated method stub
		return foodDAO.getAllFoodsCountByName(foodName);
	}

	@Override
	public Pager getFoodPager(int pageSize) {
		// TODO Auto-generated method stub
		Pager pager = new Pager();
		int count = foodDAO.getAllFoodsCount();
		pager.setPageSize(pageSize);
		pager.setRowCount(count);
		
		return pager;
	}

	@Override
	public Pager getPagerByFoodType(Integer foodTypeId, int pageSize) {
		// TODO Auto-generated method stub
		
		Pager pager = new Pager();
		int rowCount = foodDAO.getAllFoodsCountByType(foodTypeId);
		pager.setPageSize(pageSize);
		pager.setRowCount(rowCount);
		
		return pager;
	}

	@Override
	public Pager getPagerByFoodName(String name, int pageSize) {
		// TODO Auto-generated method stub
		
		Pager pager = new Pager();
		int rowCount = foodDAO.getAllFoodsCountByName(name);
		pager.setPageSize(pageSize);
		pager.setRowCount(rowCount);
		
		return pager;
	}

	@Override
	public Foods getFoodById(Integer foodId) {
		// TODO Auto-generated method stub
		return foodDAO.getFoodById(foodId);
	}

	@Override
	public void add(Foods foods) {
		// TODO Auto-generated method stub
		foodDAO.add(foods);
	}

	@Override
	public void update(Foods foods) {
		// TODO Auto-generated method stub
		foodDAO.update(foods);
	}

	@Override
	public void delete(Foods foods) {
		// TODO Auto-generated method stub
		foodDAO.delete(foods);
	}


	

}
