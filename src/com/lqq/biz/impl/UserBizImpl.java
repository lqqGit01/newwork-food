package com.lqq.biz.impl;

import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import com.lqq.biz.UserBiz;
import com.lqq.dao.UserDAO;
import com.lqq.entity.Orders;
import com.lqq.entity.Users;

@Transactional
public class UserBizImpl implements UserBiz {

	UserDAO userDAO;
	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	@Override
	public boolean checkLogin(Users users) {
		// TODO Auto-generated method stub
		return userDAO.checkLogin(users);
	}

	@Override
	public Set<Orders> getAllOrders(Integer userId) {
		// TODO Auto-generated method stub
		return userDAO.getAllOrders(userId);
	}

	@Override
	public boolean loginNameIsExist(String loginName) {
		// TODO Auto-generated method stub
		return userDAO.loginNameIsExist(loginName);
	}

	@Override
	public Users getUserByLoginName(String loginName) {
		// TODO Auto-generated method stub
		return userDAO.getUserByLoginName(loginName);
	}

	@Override
	public boolean updataUserinfo(Integer userId, String loginName, String realName, String email, String phone,
			String address) {
		// TODO Auto-generated method stub
		return userDAO.updataUserinfo(userId, loginName, realName, email, phone, address);
	}

	@Override
	public void addUser(Users user) {
		// TODO Auto-generated method stub
		userDAO.addUser(user);
	}

	

	
}
