package com.lqq.biz.impl;

import java.util.List;
import java.util.Set;
import org.springframework.transaction.annotation.Transactional;

import com.lqq.biz.OrderMainBiz;
import com.lqq.dao.OrderMainDAO;
import com.lqq.entity.Orders;
import com.lqq.entity.Pager;

@Transactional
public class OrderMainBizImpl implements OrderMainBiz{

	OrderMainDAO orderMainDAO;
	public void setOrderMainDAO(OrderMainDAO orderMainDAO) {
		this.orderMainDAO = orderMainDAO;
	}

	@Override
	public void addOrderMain(Orders orders) {
		// TODO Auto-generated method stub
		orderMainDAO.addOrderMain(orders);
	}

	@Override
	public Set<Orders> getOrdersByUserId(Integer userId) {
		// TODO Auto-generated method stub
		return orderMainDAO.getOrdersByUserId(userId);
	}

	@Override
	public void update(Orders orders) {
		// TODO Auto-generated method stub
		orderMainDAO.update(orders);
	}



	@Override
	public List<Orders> getAllOrdersByPager(int curPage, int pageSize) {
		// TODO Auto-generated method stub
		return orderMainDAO.getAllOrdersByPager(curPage, pageSize);
	}

	@Override
	public Pager getPager(int pageSize) {
		// TODO Auto-generated method stub
		Pager pager = new Pager();
		int count = orderMainDAO.getCountAllOrders();
		pager.setRowCount(count);
		pager.setPageSize(pageSize);
		return pager;
	}

	@Override
	public List<Orders> getOrderByOrderIdAndState(int curPage,int pageSize,Integer orderId, String orderState ) {
		// TODO Auto-generated method stub
		return orderMainDAO.getOrderByOrderIdAndState(curPage, pageSize, orderId, orderState);
	}

	@Override
	public Orders getOrderByOrderId(Integer orderId) {
		// TODO Auto-generated method stub
		return orderMainDAO.getOrderByOrderId(orderId);
	}

	@Override
	public Pager getPagerByOrderIdAndState(int pageSize, Integer orderId, String orderState) {
		// TODO Auto-generated method stub
		Pager pager = new Pager();
		pager.setPageSize(pageSize);
		int count = orderMainDAO.getCountOfOrdersByOrderIdAndState(orderId, orderState);
		pager.setRowCount(count);
		return pager;
	}

	@Override
	public Set<Orders> getOrdersByUserAndOrderState(String userName, String orderState) {
		// TODO Auto-generated method stub
		return orderMainDAO.getOrdersByUserAndOrderState(userName, orderState);
	}

	@Override
	public Integer getCountOfOrdersByUserAndOrderState(String userName, String orderState) {
		// TODO Auto-generated method stub
		return orderMainDAO.getCountOfOrdersByUserAndOrderState(userName, orderState);
	}

	


}
