package com.lqq.biz.impl;

import org.springframework.transaction.annotation.Transactional;

import com.lqq.biz.FoodAndFoodTypeBiz;
import com.lqq.dao.FoodAndFoodTypeDAO;

@Transactional
public class FoodAndFoodTypeBizImpl implements FoodAndFoodTypeBiz {

	FoodAndFoodTypeDAO foodAndFoodTypeDAO;
	
	public void setFoodAndFoodTypeDAO(FoodAndFoodTypeDAO foodAndFoodTypeDAO) {
		this.foodAndFoodTypeDAO = foodAndFoodTypeDAO;
	}

	@Override
	public void deleteByFoodIdAndFoodTypeId(Integer foodId, Integer foodTypeId) {
		// TODO Auto-generated method stub
		 foodAndFoodTypeDAO.deleteByFoodIdAndFoodTypeId(foodId, foodTypeId);
	}

}
