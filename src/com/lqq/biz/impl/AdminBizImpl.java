package com.lqq.biz.impl;

import org.springframework.transaction.annotation.Transactional;

import com.lqq.biz.AdminBiz;
import com.lqq.dao.AdminDAO;
import com.lqq.entity.Admins;

@Transactional
public class AdminBizImpl implements AdminBiz {

	AdminDAO adminDAO;
	
	public void setAdminDAO(AdminDAO adminDAO) {
		this.adminDAO = adminDAO;
	}

	@Override
	public boolean check_login(Admins admins) {
		// TODO Auto-generated method stub
		
		return adminDAO.check_login(admins);
	}

	@Override
	public boolean loginNameIsExist(String loginName) {
		// TODO Auto-generated method stub
		return adminDAO.loginNameIsExist(loginName);
	}

	@Override
	public boolean updateAdmin(Admins admins) {
		// TODO Auto-generated method stub
		return adminDAO.updateAdmin(admins);
	}

	@Override
	public void add(Admins admins) {
		// TODO Auto-generated method stub
		adminDAO.add(admins);
	}

}
