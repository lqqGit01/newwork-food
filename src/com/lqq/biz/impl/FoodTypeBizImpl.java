package com.lqq.biz.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.lqq.biz.FoodTypeBiz;
import com.lqq.dao.FoodTypeDAO;
import com.lqq.entity.FoodTypes;

@Transactional
public class FoodTypeBizImpl implements FoodTypeBiz {

	FoodTypeDAO foodTypeDAO;
	
	public void setFoodTypeDAO(FoodTypeDAO foodTypeDAO) {
		this.foodTypeDAO = foodTypeDAO;
	}

	@Override
	public List<FoodTypes> getAllFoodTypes() {
		// TODO Auto-generated method stub
		return foodTypeDAO.getAllFoodTypes();
	}

	@Override
	public FoodTypes getFoodTypeByFoodTypeId(Integer id) {
		// TODO Auto-generated method stub
		return foodTypeDAO.getFoodTypeByFoodTypeId(id);
	}

	@Override
	public void update(FoodTypes foodTypes) {
		// TODO Auto-generated method stub
		foodTypeDAO.update(foodTypes);
	}

}
