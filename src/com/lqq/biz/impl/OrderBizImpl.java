package com.lqq.biz.impl;


import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import com.lqq.biz.OrderBiz;
import com.lqq.dao.OrderDAO;
import com.lqq.entity.OrdersAttach;

@Transactional
public class OrderBizImpl implements OrderBiz {
	
	OrderDAO orderDAO;
	public void setOrderDAO(OrderDAO orderDAO) {
		this.orderDAO = orderDAO;
	}

	@Override
	public void addOrder(OrdersAttach ordersAttach) {
		// TODO Auto-generated method stub
		orderDAO.addOrder(ordersAttach);

	}

	@Override
	public Set<OrdersAttach> getOrdersDetailByOrderId(Integer orderId) {
		// TODO Auto-generated method stub
		return orderDAO.getOrdersDetailByOrderId(orderId);
	}


}
