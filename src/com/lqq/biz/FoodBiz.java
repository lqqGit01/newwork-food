package com.lqq.biz;

import java.util.List;
import com.lqq.entity.Foods;
import com.lqq.entity.Pager;

public interface FoodBiz {
	
		//获取制定页码的餐品列表
		public List<Foods> getFoodsByPager(int curPage,int pageSize);
		//统计所有餐品总数
		public Integer getAllFoodsCount();
		//获取餐品分页实体类
		public Pager getFoodPager(int pageSize);
		
		//根据餐品类别获取指定页码的餐品列表
		public List<Foods> getFoodsByPagerAndType(Integer foodTypeId,int curPage,int pageSize);
		//根据餐品类别获取所有餐品总数
		public Integer getAllFoodsCountByType(Integer foodTypeId );
		//根据餐品类别获取分页实体类
		public Pager getPagerByFoodType(Integer foodTypeId,int pageSize);
		
		//根据餐品名获取所有餐品列表
		public List<Foods> getFoodsByPagerAndName(String foodName,int curPage,int pageSize);
		//根据餐品名获取餐品总数
		public Integer getAllFoodsCountByName(String foodName);
		//根据餐品名获取分页实体类
		public Pager getPagerByFoodName(String name,int pageSize);
		//根据id获取餐品
		public Foods getFoodById(Integer foodId);
		
		//添加餐品
		public void add(Foods foods);
		
		//更新餐品
		public void update(Foods foods);
		
		//删除餐品
		public void delete(Foods foods);
}
