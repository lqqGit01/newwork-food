package com.lqq.interceptor;

import java.util.Map;

import com.lqq.entity.Users;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

@SuppressWarnings("serial")
public class LoginInterceptor extends AbstractInterceptor {

	@Override
	public String intercept(ActionInvocation arg0) throws Exception {
		// TODO Auto-generated method stub
		
		ActionContext actionContext = arg0.getInvocationContext();
		Map<String, Object> sessionMap = actionContext.getSession();
		Users users = (Users) sessionMap.get("user");
		if(users == null){
			actionContext.put("tip", "����û��¼,���ȵ�¼!");
			return Action.LOGIN;
		}
		
		return arg0.invoke();
	}

}
