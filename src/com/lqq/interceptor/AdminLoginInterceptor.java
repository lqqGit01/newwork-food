package com.lqq.interceptor;

import java.util.Map;

import com.lqq.entity.Admins;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class AdminLoginInterceptor extends AbstractInterceptor {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		// TODO Auto-generated method stub
		ActionContext context = invocation.getInvocationContext();
		Map<String, Object> sessionMap = context.getSession();
		Admins admin = (Admins) sessionMap.get("admin");
		if(admin == null){
			context.put("tip", "管理员操作之前,请先登录!");
			return Action.LOGIN;
		}
		return invocation.invoke();
	}

}
